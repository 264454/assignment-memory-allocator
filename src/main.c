#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>


int main(){

    heap_init(1000);
    void *block1 = _malloc(100);
    debug_heap(stdout, HEAP_START);

    void *block2 = _malloc(300);
    void *block3 = _malloc(500);
    debug_heap(stdout, HEAP_START);

    _free(block1);
    void *block4 = _malloc(50);
    debug_heap(stdout, HEAP_START);

    _free(block2);
    void *block5 = _malloc(310);
    debug_heap(stdout, HEAP_START);

    void *block6 = _malloc(200);
    debug_heap(stdout, HEAP_START);

    _free(block3);
    _free(block4);
    _free(block5);
    _free(block6);
}
