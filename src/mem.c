#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void* const addr, size_t query ) {
  size_t query_cap = size_max(BLOCK_MIN_CAPACITY, query);
  size_t actual_size = region_actual_size(offsetof(struct block_header, contents) + query_cap);
  void* m_page = map_pages(addr, actual_size, MAP_FIXED);
  bool stat = true;
  if (m_page == MAP_FAILED) {
    m_page = map_pages(addr, actual_size, 0);
    if (m_page == MAP_FAILED) return REGION_INVALID;
    stat = false;
  }
  block_init(m_page, (block_size) {.bytes = actual_size}, NULL);
  return (struct region) {.addr=m_page, .size = actual_size, .extends = stat};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(!block_splittable(block, query)) return false;
  query = size_max(BLOCK_MIN_CAPACITY, query);
  block_size cap_rem = {block->capacity.bytes - query};
  block->capacity = (block_capacity) {.bytes = query};
  void* next_part = block_after(block);
  block_init(next_part, cap_rem, block->next);
  block->next = next_part;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if(block->next == NULL || !mergeable(block, block->next)) return false;
  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  while(block->next != NULL) {
    while(try_merge_with_next(block));
    if(block->is_free && block_is_big_enough(sz, block)) return (struct block_search_result) {.block = block, .type = BSR_FOUND_GOOD_BLOCK};
    block = block->next;
  }
  if(block->is_free && block_is_big_enough(sz, block)) return (struct block_search_result) {.block = block, .type = BSR_FOUND_GOOD_BLOCK};
  return (struct block_search_result) {.block = block, .type = BSR_REACHED_END_NOT_FOUND};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result rez = find_good_or_last(block, query);
  if(rez.type != BSR_FOUND_GOOD_BLOCK) return rez;
  split_if_too_big(rez.block, query);
  rez.block->is_free = false;
  return rez;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct region grown = alloc_region(block_after(last),  query);
  if (region_is_invalid(&grown)) return NULL;
  last->next = grown.addr;
  if (try_merge_with_next(last)) return last;
  return grown.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  struct block_search_result good_or_last = try_memalloc_existing(query, heap_start);
  if(good_or_last.type == BSR_FOUND_GOOD_BLOCK) return good_or_last.block;
  struct block_header* grown = grow_heap(good_or_last.block, query);
  if(grown == NULL) return NULL;
  struct block_search_result new_block = try_memalloc_existing(query, grown);
  if(new_block.type == BSR_FOUND_GOOD_BLOCK) return new_block.block;
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
